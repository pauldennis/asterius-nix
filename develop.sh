set +e

BEGINDATE=$(date --rfc-3339=s)

./do_download.sh

nix-build ./local --show-trace

notify-send "done"

echo "begin"
echo $BEGINDATE
echo end
date --rfc-3339=s
