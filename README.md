# asterius-nix

build the `Haskell` to `WebAssembly` compiler called [asterius](https://github.com/tweag/asterius) within a nix expression.

# usage

See [asterius-nix-usage-template](https://gitlab.com/pauldennis/asterius-nix-usage-template) for how to invoke the nix expression.
