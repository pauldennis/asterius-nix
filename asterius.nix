{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, stdenv ? pkgs.stdenv

, git ? pkgs.git
, quickserve ? pkgs.quickserve
, curl ? pkgs.curl
, python ? pkgs.python3
, stack ? pkgs.stack
, autoconf ? pkgs.autoconf
, automake ? pkgs.automake
, ncurses ? pkgs.ncurses
, glibcLocales ? pkgs.glibcLocales
, zlib ? pkgs.zlib
, nodejs ? pkgs.nodejs
, cabalCli ? pkgs.cabal-install

, wasiSdk ? import sources.wasiSdk

, toBeCompiled ? ./compileTestCase
, additionalCabalPackages ? ./additionalCabalPackagesEmptyFolder

, dependencySnapshot

}:


stdenv.mkDerivation {
  name = "asterius";
  builder = ./builder.sh;

  buildInputs = [
    git
    quickserve
    curl
    python
    stack
    autoconf
    automake
    ncurses
    glibcLocales
    zlib
    nodejs
    cabalCli
  ];

  ghc884
    = let
      pkgs = import (builtins.fetchGit {
        # Descriptive name to make the store path easier to identify
        name = "ghc884";
        url = "https://github.com/NixOS/nixpkgs/";
        ref = "refs/heads/nixpkgs-unstable";
        rev = "2c162d49cd5b979eb66ff1653aecaeaa01690fcc";
      }) {};
        myPkg = pkgs.ghc;
      in myPkg;

  #https://github.com/commercialhaskell/stack/issues/793
  LANG = "en_US.utf8";

  inherit wasiSdk;

  inherit toBeCompiled;
  inherit additionalCabalPackages;

  preventInternetAccess = ./patches;

  inherit dependencySnapshot;

}
