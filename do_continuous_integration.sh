set -e

echo "install things"
nix-env -iA nixpkgs.git
nix-env -iA nixpkgs.curl
nix-env -iA nixpkgs.gnutar
nix-env -iA nixpkgs.wget

echo "do download"
./do_download.sh

# echo "do nix-build"
# nix-build ./local.nix --show-trace
