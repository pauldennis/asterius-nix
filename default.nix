{
  toBeCompiled ? ./compileTestCase
, additionalCabalPackages ? ./additionalCabalPackagesEmptyFolder

}:

import ./asterius.nix
{
  dependencySnapshot=builtins.fetchurl
  {
    url = https://gitlab.com/pauldennis/asterius-nix/-/package_files/8173533/download;
    sha256 = "0xg922xb9g17wp0568ci98wm2zvhjnnc7p72mga8zqivni34jxcq";
  };
  inherit toBeCompiled;
  inherit additionalCabalPackages;
}
