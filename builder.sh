echo "START###############################################"

# sets up all the convienience commands for building
source $stdenv/setup

STARTTIME=$(date --rfc-3339=s)
echo $STARTTIME

# stack trys to link things in the ~/.stack folder
# the linker wants everything to be in the nix store
HOME=$out/homeless-shelter

# get build dependencies into scope
PATH=$ghc884/bin:$PATH

WASI_SDK_PATH=$wasiSdk/wasi-sdk/build/install/opt/wasi-sdk
export WASI_SDK_PATH

# setup an http server inorder to convince stack to use local files
quickserve . &
echo "beacon" > ./beacon

# wait for the server to become ready
# TODO wait until quickserve is ready instead of discret amount of seconds
for i in {0..60}; do
  curl http://127.0.0.1:8000/build/beacon -o ./beacon_down || true
  if cmp -s ./beacon ./beacon_down; then
    break
  else
    echo "trying out quickserve failed, wait $i seconds"
    sleep $i
  fi
done

# The output is supposed to be installed into this location
# apparently the folder does not exist yet so we create it
mkdir $out

mkdir $out/homeless-shelter/
touch $out/homeless-shelter/there-are-hidden-files-in-this-folder

# copy to usable location
tar xfz $dependencySnapshot

cd /build/ghc/
  cp $preventInternetAccess/ghc.diff ./
  git apply ghc.diff

cd /build/asterius/
  cp $preventInternetAccess/asterius.diff ./
  git apply asterius.diff

cp --no-clobber $additionalCabalPackages/* /build/packagesForCabal/

# convince stack not to use internet
mkdir ~/.stack/
cp $preventInternetAccess/config.yaml ~/.stack/config.yaml

cp /build/github.com/commercialhaskell/stackage-content/raw/master/stack/stack-setup-2.yaml ~/.stack/stack-setup-2.yaml

cd $out
cp -r /build/asterius ./

cd $out/asterius
  echo "now compile ghc"
  # instructions according to
  # https://asterius.netlify.app/building.html
    mkdir lib
    pushd lib

    # do the git clone mannualy
    mv /build/ghc ./

    python ../utils/make-packages.py
    rm -rf ghc
    popd
  echo "finished to compile ghc"

  echo "now build asterius itself"
  stack build asterius
  echo "finished to compile asterius"

  echo "now do the booting"
  stack exec ahc-boot
  echo "finished booting"

# actual compiling
mkdir $out/compile-output
mkdir $out/asterius/test/
cd $out/asterius/test/

  cp -r $toBeCompiled/* ./

  echo "now compile"
  stack exec ahc-cabal -- new-install . --installdir .
  echo "finished compiling"

  echo "now extract wasm"
  stack exec ahc-dist -- --browser --input-exe Hilbert --output-directory $out/compile-output/
  echo "finished extracting wasm"
cd ..



echo "STOP################################################"

echo "begin time:"
echo $STARTTIME
echo "end time:"
date --rfc-3339=s
