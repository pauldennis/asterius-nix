set -e

echo "begin downloading"

rm -rf ./asterius-dependency-snapshot/
mkdir -p ./asterius-dependency-snapshot/

download_if_not_local () {
  wget -r --no-clobber $1
}

download_for_ahc_cabal () {
NAME=$1
VERSION=$2
mkdir -p packagesForCabal
cd packagesForCabal
  wget --no-clobber "http://hackage.haskell.org/package/$NAME-$VERSION/$NAME-$VERSION.tar.gz"
  wget --no-clobber "http://hackage.haskell.org/package/$NAME-$VERSION/$NAME.cabal"

  cp "$NAME.cabal" "$NAME-$VERSION.cabal"
cd ..
}

cd ./asterius-dependency-snapshot/

  git clone --recursive "https://github.com/tweag/asterius.git" --branch master --single-branch
  git clone --recursive "https://github.com/TerrorJack/ghc.git" --branch asterius-8.8-staging --single-branch
  rm -rf ./asterius/.git/
  rm -rf ./ghc/.git/


  download_if_not_local "https://github.com/commercialhaskell/stackage-content/raw/master/stack/stack-setup-2.yaml"

  download_if_not_local "https://raw.githubusercontent.com/commercialhaskell/stackage-snapshots/master/lts/16/29.yaml"

  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/root.json"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/timestamp.json"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/snapshot.json"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/mirrors.json"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/01-index.tar.gz"

  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/alex-3.2.6.tar.gz "
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/clock-0.8.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/hashable-1.3.0.0.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/happy-1.19.12.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/extra-1.7.9.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/heaps-0.3.6.1.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/js-dgtable-0.5.2.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/filepattern-0.1.2.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/js-dgtable-0.5.2.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/js-flot-0.8.3.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/js-jquery-3.3.1.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/primitive-0.7.0.1.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/random-1.1.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/unordered-containers-0.2.10.0.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/splitmix-0.0.5.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/utf8-string-1.0.1.1.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/QuickCheck-2.13.2.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/shake-0.19.2.tar.gz"

  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/binaryen-0.0.6.0.tar.gz"

  download_if_not_local "github.com/tweag/inline-js/archive/ef675745e84d23d51c50660d40acf9e684fbb2d6.tar.gz"

  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/array-0.5.4.0.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/deepseq-1.4.4.0.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/time-1.9.3.tar.gz"
  download_if_not_local "s3.amazonaws.com/hackage.fpcomplete.com/package/bytestring-0.10.10.1.tar.gz"

  download_for_ahc_cabal unix 2.7.2.2
  download_for_ahc_cabal filepath 1.4.2.1
  download_for_ahc_cabal time 1.9.3

  download_for_ahc_cabal bytestring 0.10.10.1
  download_for_ahc_cabal deepseq 1.4.4.0
  download_for_ahc_cabal array 0.5.4.0
  download_for_ahc_cabal test-framework 0.8.2.0
  download_for_ahc_cabal test-framework-hunit 0.3.0.2
  download_for_ahc_cabal HUnit 1.6.1.0
  download_for_ahc_cabal random 1.1
  download_for_ahc_cabal containers 0.6.2.1
  download_for_ahc_cabal ansi-terminal 0.10.3
  download_for_ahc_cabal ansi-wl-pprint 0.6.9
  download_for_ahc_cabal regex-posix 0.96.0.0
  download_for_ahc_cabal old-locale 1.0.0.7
  download_for_ahc_cabal xml 1.3.14
  download_for_ahc_cabal hostname 1.0

  download_for_ahc_cabal binary 0.8.7.0
  download_for_ahc_cabal directory 1.3.6.0
  download_for_ahc_cabal mtl 2.2.2
  download_for_ahc_cabal pretty 1.1.3.6
  download_for_ahc_cabal transformers 0.5.6.2
  download_for_ahc_cabal aeson 1.4.7.1
  download_for_ahc_cabal parsec 3.1.14.0
  download_for_ahc_cabal vector 0.12.1.2
  download_for_ahc_cabal uuid-types 1.0.3
  download_for_ahc_cabal th-abstraction 0.3.2.0
  download_for_ahc_cabal scientific 0.3.6.2
  download_for_ahc_cabal hashable 1.3.0.0
  download_for_ahc_cabal dlist 0.8.0.8
  download_for_ahc_cabal attoparsec 0.13.2.4
  download_for_ahc_cabal unordered-containers 0.2.10.0
  download_for_ahc_cabal time-compat 1.9.5
  download_for_ahc_cabal base-orphans 0.8.4
  download_for_ahc_cabal text 1.2.4.0
  download_for_ahc_cabal tagged 0.8.6.1
  download_for_ahc_cabal integer-logarithms 1.0.3.1
  download_for_ahc_cabal primitive 0.7.0.1
  download_for_ahc_cabal base-compat-batteries 0.11.2
  download_for_ahc_cabal base-compat 0.11.2

  download_for_ahc_cabal svg-builder 0.1.1
  download_for_ahc_cabal blaze-builder 0.4.1.0
  download_for_ahc_cabal lucid-svg 0.7.1
  download_for_ahc_cabal lucid 2.9.12
  download_for_ahc_cabal mmorph 1.1.3
  download_for_ahc_cabal transformers-compat 0.6.6
  download_for_ahc_cabal diagrams-svg 1.4.3
  download_for_ahc_cabal split 0.2.3.4
  download_for_ahc_cabal semigroups 0.19.1
  download_for_ahc_cabal optparse-applicative 0.15.1.0
  download_for_ahc_cabal process 1.6.9.0
  download_for_ahc_cabal monoid-extras 0.5.1
  download_for_ahc_cabal semigroupoids 5.3.4
  download_for_ahc_cabal contravariant 1.5.3
  download_for_ahc_cabal StateVar 1.2.1
  download_for_ahc_cabal stm 2.5.0.0
  download_for_ahc_cabal distributive 0.6.2.1
  download_for_ahc_cabal comonad 5.0.6
  download_for_ahc_cabal bifunctors 5.5.7
  download_for_ahc_cabal groups 0.4.1.0
  download_for_ahc_cabal lens 4.18.1
  download_for_ahc_cabal type-equality 1
  download_for_ahc_cabal reflection 2.1.6
  download_for_ahc_cabal profunctors 5.5.2
  download_for_ahc_cabal parallel 3.2.2.0
  download_for_ahc_cabal kan-extensions 5.2.1
  download_for_ahc_cabal invariant 0.5.3
  download_for_ahc_cabal adjunctions 4.4
  download_for_ahc_cabal void 0.7.3
  download_for_ahc_cabal free 5.1.3
  download_for_ahc_cabal transformers-base 0.4.5.2
  download_for_ahc_cabal exceptions 0.10.4
  download_for_ahc_cabal call-stack 0.2.0
  download_for_ahc_cabal diagrams-core 1.4.2
  download_for_ahc_cabal linear 1.21.3
  download_for_ahc_cabal cereal 0.5.8.1
  download_for_ahc_cabal bytes 0.17
  download_for_ahc_cabal binary-orphans 1.0.1
  download_for_ahc_cabal dual-tree 0.2.2.1
  download_for_ahc_cabal colour 2.3.5
  download_for_ahc_cabal shelly 1.9.0
  download_for_ahc_cabal constraints 0.12
  download_for_ahc_cabal fsnotify 0.3.0.1
  download_for_ahc_cabal diagrams-lib 1.4.3
  download_for_ahc_cabal lifted-async 0.10.1.2
  download_for_ahc_cabal diagrams 1.4

  download_for_ahc_cabal newtype-generics 0.5.4
  download_for_ahc_cabal base64-bytestring 1.0.0.3

  download_for_ahc_cabal diagrams-solve 0.1.2
  download_for_ahc_cabal active 0.2.0.14
  download_for_ahc_cabal data-default-class 0.1.2.0
  download_for_ahc_cabal fingertree 0.1.4.2
  download_for_ahc_cabal intervals 0.9.1
  download_for_ahc_cabal JuicyPixels 3.3.5
  download_for_ahc_cabal zlib 0.6.2.2
  download_for_ahc_cabal async 2.2.2
  download_for_ahc_cabal unix-compat 0.5.2
  download_for_ahc_cabal monad-control 1.0.2.3
  download_for_ahc_cabal hinotify 0.4.1
  download_for_ahc_cabal lifted-base 0.2.3.12
  download_for_ahc_cabal enclosed-exceptions 1.0.3
  download_for_ahc_cabal data-default-instances-old-locale 0.0.1
  download_for_ahc_cabal data-default-instances-dlist 0.0.1
  download_for_ahc_cabal data-default-instances-containers 0.0.1
  download_for_ahc_cabal MemoTrie 0.6.10

  download_for_ahc_cabal diagrams-core 1.4.2
  download_for_ahc_cabal diagrams-lib 1.4.3
  download_for_ahc_cabal diagrams-contrib 1.4.4

  download_for_ahc_cabal force-layout 0.4.0.6
  download_for_ahc_cabal data-default 0.7.1.1
  download_for_ahc_cabal MonadRandom 0.5.2
  download_for_ahc_cabal circle-packing 0.1.0.6
  download_for_ahc_cabal cubicbezier 0.6.0.6
  download_for_ahc_cabal mfsolve 0.3.2.0
  download_for_ahc_cabal integration 0.2.1
  download_for_ahc_cabal matrices 0.5.0
  download_for_ahc_cabal microlens 0.4.11.2
  download_for_ahc_cabal microlens-th 0.4.3.6
  download_for_ahc_cabal microlens-mtl 0.2.0.1
  download_for_ahc_cabal fast-math 1.0.2
  download_for_ahc_cabal vector-space 0.16
  download_for_ahc_cabal mtl-compat 0.2.2
  download_for_ahc_cabal Boolean 0.2.4
  download_for_ahc_cabal NumInstances 1.4
  download_for_ahc_cabal newtype-generics 0.5.4

  download_for_ahc_cabal cabal-doctest 1.0.8
  download_for_ahc_cabal Cabal 3.0.1.0

cd ..

echo "put into archive"
tar --sort=name --owner=root:0 --group=root:0 --mtime='UTC 2019-01-01' -czf asterius-nix-dependency-snapshot.tar.gz --directory=./asterius-dependency-snapshot/ .

echo "finished downloading"

echo "the hash is:"
nix-hash --type sha256 --flat --base32 asterius-nix-dependency-snapshot.tar.gz

echo "move to packaging area"
mv asterius-nix-dependency-snapshot.tar.gz ../internetSnapshotArtifact/
